package com.example.browserapp

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.example.browserapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        titleWebView()

        binding.webview.webViewClient = WebViewClient()

        binding.searchBtn.setOnClickListener {
            val link = binding.linkTxt.text.toString()
            binding.webview.loadUrl(link)
        }

    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            menuInflater.inflate(R.menu.menu_main_land, menu)
        } else {
            menuInflater.inflate(R.menu.menu_main, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.green -> changeButtonColor(Color.GREEN)
            R.id.blue -> changeButtonColor(Color.BLUE)
            R.id.yellow -> changeButtonColor(Color.YELLOW)
            R.id.cyan -> changeButtonColor(Color.CYAN)
            R.id.magenta -> changeButtonColor(Color.MAGENTA)
            R.id.gray -> changeButtonColor(Color.GRAY)
            else -> super.onOptionsItemSelected(item)
        }
        return true
    }
    private fun changeButtonColor(color: Int) {
        binding.searchBtn.backgroundTintList = ColorStateList.valueOf(color)
    }

    private fun titleWebView() {
        binding.webview.apply {
            webViewClient = WebViewClient()
            webChromeClient = object : WebChromeClient() {
                override fun onReceivedTitle(view: WebView?, title: String?) {
                    binding.toolbar.title = title ?: getString(R.string.app_name)
                }
            }
        }
    }
}